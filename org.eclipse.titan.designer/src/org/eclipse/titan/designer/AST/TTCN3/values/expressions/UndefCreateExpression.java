/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.Value;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * UndefCreateExpression represents an expression in the source code that is 
 * either a create operation or a constructor call.
 * 
 * Ttcn grammar is ambiguous for some cases and the parser is unable to 
 * differentiate between create op and constructor call in all circumstances, so we need to
 * store parsed data in a temporary form to postpone the decision for 
 * the semantic analisys phase.
 *  
 * @author Miklos Magyari
 */

public class UndefCreateExpression extends Expression_Value {
	private final Reference reference;
	private final Value name;
	private final Value location;
	private final boolean isAlive;
	private ComponentCreateExpression component;

	private boolean isComponent = false;
	
	public UndefCreateExpression(final Reference reference, final Value name, final Value location, final boolean isAlive) {
		this.reference = reference;
		this.name = name;
		this.location = location;
		this.isAlive = isAlive;
		component = new ComponentCreateExpression(reference, name, location, isAlive);
		
		if (reference != null) {
			reference.setFullNameParent(this);
		}
		if (name != null) {
			name.setFullNameParent(this);
		}
		if (location != null) {
			location.setFullNameParent(this);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (reference != null) {
			reference.setMyScope(scope);
		}
		if (name != null) {
			name.setMyScope(scope);
		}
		if (location != null) {
			location.setMyScope(scope);
		}
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.COMPONENT_CREATE_OPERATION;
	}

	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		IType type = getOwnType(timestamp);
		if (type instanceof Component_Type) {
			return component.evaluateValue(timestamp, expectedValue, referenceChain);
		}
		return null;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		return true;
	}

	@Override
	public String createStringRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setCodeSection(final CodeSectionType codeSection) {
		super.setCodeSection(codeSection);

		if (reference != null) {
			reference.setCodeSection(codeSection);
		}
		if (name != null) {
			name.setCodeSection(codeSection);
		}
		if (location != null) {
			location.setCodeSection(codeSection);
		}
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		IType type = getOwnType(timestamp);
		if (type instanceof Component_Type) {
			return component.getExpressionReturntype(timestamp, expectedValue);
		}
		return null;
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	/** {@inheritDoc} */
	public Type getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		IType type = getOwnType(timestamp);
		if (type instanceof Component_Type) {
			isComponent = true;
			return component.getExpressionGovernor(timestamp, expectedValue);
		}
		return null;
	}
	
	private IType getOwnType(CompilationTimeStamp timestamp) {
		Assignment assignment = reference.getRefdAssignment(timestamp, false);
		return assignment.getType(timestamp);
	}
}
